## taimen-user 11 RP1A.201005.004.A1 6934943 release-keys
- Manufacturer: google
- Platform: msm8998
- Codename: taimen
- Brand: google
- Flavor: taimen-user
- Release Version: 11
- Id: RP1A.201005.004.A1
- Incremental: 6934943
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/taimen/taimen:11/RP1A.201005.004.A1/6934943:user/release-keys
- OTA version: 
- Branch: taimen-user-11-RP1A.201005.004.A1-6934943-release-keys
- Repo: google_taimen_dump_1470


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
